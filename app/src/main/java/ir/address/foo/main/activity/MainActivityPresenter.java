package ir.address.foo.main.activity;

import com.google.android.gms.maps.model.LatLng;

import ir.address.foo.app.Injection;

public class MainActivityPresenter {
    private MainActivityView view;

    public MainActivityPresenter(MainActivityView view){
        this.view = view;
    }

    public void loadCenterOfTehranPoints(){
        LatLng centerOfTehran = new LatLng(35.6935203, 51.3942589);
        Injection.provideNearbyDataRepository().loadPoints(centerOfTehran);
    }
}
