package ir.address.foo.main.activity;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ir.address.foo.R;
import ir.address.foo.app.Injection;
import ir.address.foo.base.ui.fragments.BaseFragment;
import ir.address.foo.base.ui.activities.BaseActivity;
import ir.address.foo.base.util.Utils;
import ir.address.foo.main.fragment.MainFragment;
import ir.address.foo.nearby.fragment.NearbyFragment;

public class MainActivity extends BaseActivity implements MainActivityView {

    private FragmentManager manager;
    private MainActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set locate
        Utils.setFarsiLocate(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter = new MainActivityPresenter(MainActivity.this);
                presenter.loadCenterOfTehranPoints();
            }
        }, 3000);

        // commit fragment
        setContentView(R.layout.activity_main);
        manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        BaseFragment fragment = MainFragment.getInstance();
        transaction.replace(R.id.fragment_container, fragment, MainFragment.TAG);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        // handle back press in nearby fragment
        Fragment mainFragment = manager.findFragmentByTag(MainFragment.TAG);
        if (mainFragment != null) {
            boolean handled = ((MainFragment) mainFragment).onBackPressed();
            if (handled) {
                return;
            }
        }
        super.onBackPressed();
    }
}
