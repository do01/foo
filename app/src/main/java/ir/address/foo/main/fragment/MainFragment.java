package ir.address.foo.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import ir.address.foo.R;
import ir.address.foo.base.ui.fragments.BaseFragment;
import ir.address.foo.filter.FilterFragment;
import ir.address.foo.main.fragment.listeners.OnFilterChangeListener;
import ir.address.foo.nearby.fragment.NearbyFragment;

public class MainFragment extends BaseFragment implements OnFilterChangeListener {
    public static final String TAG = MainFragment.class.getSimpleName();

    @BindView(R.id.filter_fragment_container)
    FrameLayout filterFragmentContainer;
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    private FragmentManager mManager;

    public static MainFragment getInstance(){
        MainFragment mainFragment = new MainFragment();
        mainFragment.setArguments(new Bundle());
        return mainFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_main, container, false);

        // commit filter fragment
        mManager = getChildFragmentManager();
        FragmentTransaction transaction = mManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        BaseFragment filterFragment = FilterFragment.getInstance();
        transaction.replace(R.id.filter_fragment_container, filterFragment, FilterFragment.TAG);
        transaction.commitAllowingStateLoss();

        // commit nearby fragment
        mManager = getChildFragmentManager();
        transaction = mManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        BaseFragment nearbyFragment = NearbyFragment.getInstance();
        transaction.replace(R.id.fragment_container, nearbyFragment, NearbyFragment.TAG);
        transaction.commitAllowingStateLoss();

        return view;
    }

    @Override
    public void onFilterChange(String FilterName) {

    }

    @Override
    public boolean onBackPressed() {
        Fragment nearbyFragment = mManager.findFragmentByTag(NearbyFragment.TAG);
        if (nearbyFragment != null) {
            return ((NearbyFragment) nearbyFragment).onBackPressed();
        }
        return super.onBackPressed();
    }
}
