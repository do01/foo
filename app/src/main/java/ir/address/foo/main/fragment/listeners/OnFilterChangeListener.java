package ir.address.foo.main.fragment.listeners;

public interface OnFilterChangeListener {
    void onFilterChange(String FilterName);
}
