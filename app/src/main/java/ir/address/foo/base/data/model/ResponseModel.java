package ir.address.foo.base.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseModel<T> implements Serializable {

    @SerializedName("highways")
    private List<T> highways = new ArrayList<>();
    @SerializedName("bus_stops")
    private List<T> busStops = new ArrayList<>();
    @SerializedName("metro_stations")
    private List<T> metroStations = new ArrayList<>();
    @SerializedName("cafes")
    private List<T> cafes = new ArrayList<>();
    @SerializedName("restaurants")
    private List<T> restaurants = new ArrayList<>();
    @SerializedName("parks")
    private List<T> parks = new ArrayList<>();
    @SerializedName("shopping_malls")
    private List<T> shoppingMalls = new ArrayList<>();
    @SerializedName("mosques")
    private List<T> mosques = new ArrayList<>();
    @SerializedName("pharmacies")
    private List<T> pharmacies = new ArrayList<>();
    @SerializedName("hospitals")
    private List<T> hospitals = new ArrayList<>();
    @SerializedName("schools")
    private List<T> schools = new ArrayList<>();
    @SerializedName("gyms")
    private List<T> gyms = new ArrayList<>();
    @SerializedName("bookstores")
    private List<T> bookstores = new ArrayList<>();
    @SerializedName("flower_shops")
    private List<T> flowerShops = new ArrayList<>();
    @SerializedName("libraries")
    private List<T> libraries = new ArrayList<>();
    @SerializedName("hotels")
    private List<T> hotels = new ArrayList<>();
    @SerializedName("parkings")
    private List<T> parkings = new ArrayList<>();

    public List<T> getHighways() {
        return highways;
    }

    public void setHighways(List<T> highways) {
        this.highways = highways;
    }

    public List<T> getBusStops() {
        return busStops;
    }

    public void setBusStops(List<T> busStops) {
        this.busStops = busStops;
    }

    public List<T> getMetroStations() {
        return metroStations;
    }

    public void setMetroStations(List<T> metroStations) {
        this.metroStations = metroStations;
    }

    public List<T> getCafes() {
        return cafes;
    }

    public void setCafes(List<T> cafes) {
        this.cafes = cafes;
    }

    public List<T> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<T> restaurants) {
        this.restaurants = restaurants;
    }

    public List<T> getParks() {
        return parks;
    }

    public void setParks(List<T> parks) {
        this.parks = parks;
    }

    public List<T> getShoppingMalls() {
        return shoppingMalls;
    }

    public void setShoppingMalls(List<T> shoppingMalls) {
        this.shoppingMalls = shoppingMalls;
    }

    public List<T> getMosques() {
        return mosques;
    }

    public void setMosques(List<T> mosques) {
        this.mosques = mosques;
    }

    public List<T> getPharmacies() {
        return pharmacies;
    }

    public void setPharmacies(List<T> pharmacies) {
        this.pharmacies = pharmacies;
    }

    public List<T> getHospitals() {
        return hospitals;
    }

    public void setHospitals(List<T> hospitals) {
        this.hospitals = hospitals;
    }

    public List<T> getSchools() {
        return schools;
    }

    public void setSchools(List<T> schools) {
        this.schools = schools;
    }

    public List<T> getGyms() {
        return gyms;
    }

    public void setGyms(List<T> gyms) {
        this.gyms = gyms;
    }

    public List<T> getBookstores() {
        return bookstores;
    }

    public void setBookstores(List<T> bookstores) {
        this.bookstores = bookstores;
    }

    public List<T> getFlowerShops() {
        return flowerShops;
    }

    public void setFlowerShops(List<T> flowerShops) {
        this.flowerShops = flowerShops;
    }

    public List<T> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<T> libraries) {
        this.libraries = libraries;
    }

    public List<T> getHotels() {
        return hotels;
    }

    public void setHotels(List<T> hotels) {
        this.hotels = hotels;
    }

    public List<T> getParkings() {
        return parkings;
    }

    public void setParkings(List<T> parkings) {
        this.parkings = parkings;
    }
}
