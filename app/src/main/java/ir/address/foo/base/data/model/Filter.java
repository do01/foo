package ir.address.foo.base.data.model;

import java.util.List;

public class Filter {
    private String titleFa;
    private PointType type;
    private List<BasePoint> data;

    public Filter(){}

    public Filter(String titleFa, PointType type){
        this.titleFa = titleFa;
        this.type = type;
    }

    public Filter(String titleFa, PointType type, List<BasePoint> points){
        this(titleFa, type);
        data = points;
    }

    public String getTitleFa() {
        return titleFa;
    }

    public void setTitleFa(String titleFa) {
        this.titleFa = titleFa;
    }

    public PointType getType() {
        return type;
    }

    public void setType(PointType type) {
        this.type = type;
    }

    public List<BasePoint> getData() {
        return data;
    }

    public void setData(List<BasePoint> data) {
        this.data = data;
    }
}
