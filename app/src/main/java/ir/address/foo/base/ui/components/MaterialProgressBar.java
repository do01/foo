package ir.address.foo.base.ui.components;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ProgressBar;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import ir.address.foo.R;


public class MaterialProgressBar extends ProgressBar {
    // Same dimensions as medium-sized native Material progress bar
    private static final int RADIUS_DP = 16;
    private static final int WIDTH_DP = 4;

    public MaterialProgressBar(Context context) {
        super(context);
        init(context);
    }

    public MaterialProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MaterialProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MaterialProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context){
        // set tint color
        getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.MULTIPLY );
        // The version range is more or less arbitrary - you might want to modify it
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP
                || Build.VERSION.SDK_INT > Build.VERSION_CODES.O_MR1) {

            final DisplayMetrics metrics = getResources().getDisplayMetrics();
            final float screenDensity = metrics.density;

            CircularProgressDrawable drawable = new CircularProgressDrawable(context);
            drawable.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
            drawable.setCenterRadius(RADIUS_DP * screenDensity);
            drawable.setStrokeWidth(WIDTH_DP * screenDensity);
            setIndeterminateDrawable(drawable);
        }
    }
}
