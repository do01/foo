package ir.address.foo.base.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ir.address.foo.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static final String BASE_URL = "https://bo.hichestan.org/rest/";
    private static ApiService INSTANCE;

    private Retrofit retrofit;

    private ApiService(){
        Gson gson = (new GsonBuilder()).create();
        Retrofit.Builder builder = new Retrofit.Builder();
        retrofit = builder
                .baseUrl(BASE_URL)
                .client(provideOkHttpClient(provideLoggingInterceptor()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static ApiService getInstance(){
        if(INSTANCE == null){
            INSTANCE = new ApiService();
        }
        return INSTANCE;
    }

    private OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor);
        return builder.build();
    }

    private HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if(BuildConfig.DEBUG) {
            interceptor.setLevel(Level.BODY);
        }else{
            interceptor.setLevel(Level.NONE);
        }
        return interceptor;
    }

    public <T> T create(Class<T> service){
        return retrofit.create(service);
    }
}