package ir.address.foo.base.data.repository;


import com.google.android.gms.maps.model.LatLng;

import java.util.Observable;

import ir.address.foo.app.Injection;
import ir.address.foo.base.data.api.general.GeneralApiRepository;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.base.data.model.PointType;
import ir.address.foo.base.data.model.ResponseModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyDataRepository extends Observable {

    private static NearbyDataRepository INSTANCE;
    private GeneralApiRepository generalRepository;
    private Filter selectedFilter;
    private Call<ResponseModel<BasePoint>> getPointsCall;
    private ResponseModel<BasePoint> latestResponseModel;

    private NearbyDataRepository(){
        generalRepository = Injection.provideGeneralApiRepository();

        // default filter
        Filter defaultFilter = new Filter();
        defaultFilter.setType(PointType.RESTAURANTS);
        selectedFilter = defaultFilter;
    }

    public static NearbyDataRepository getInstance(){
        if(INSTANCE == null){
            INSTANCE = new NearbyDataRepository();
        }
        return INSTANCE;
    }

    public void loadPoints(LatLng latLng){
        // cancel if running
        if(getPointsCall != null && !getPointsCall.isExecuted()){
            getPointsCall.cancel();
        }

        getPointsCall = Injection.provideGeneralApiRepository()
                .getPoints(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
        getPointsCall.enqueue(new Callback<ResponseModel<BasePoint>>() {
            @Override
            public void onResponse(Call<ResponseModel<BasePoint>> call, Response<ResponseModel<BasePoint>> response) {
                if (response.body() != null) {
                    latestResponseModel = response.body();
                    setChanged();
                    pushFilteredData(response.body());
                    setChanged();
                    notifyObservers(response.body());
                }
            }
            @Override
            public void onFailure(Call<ResponseModel<BasePoint>> call, Throwable t) {}
        });
    }

    private void pushFilteredData(ResponseModel<BasePoint> responseModel){
        switch (selectedFilter.getType()){
            case RESTAURANTS:
                selectedFilter.setData(responseModel.getRestaurants());
                selectedFilter.setTitleFa("رستوران ها");
                break;
            case CAFES:
                selectedFilter.setData(responseModel.getCafes());
                selectedFilter.setTitleFa("کافه ها");
                break;
            case HIGHWAYS:
                selectedFilter.setData(responseModel.getHighways());
                selectedFilter.setTitleFa("بزرگراه ها");
                break;
            case BUS_STOPS:
                selectedFilter.setData(responseModel.getBusStops());
                selectedFilter.setTitleFa("ایستگاه های اتوبوس");
                break;
            case METRO_STATIONS:
                selectedFilter.setData(responseModel.getMetroStations());
                selectedFilter.setTitleFa("ایستگاه های مترو");
                break;
            case HOTELS:
                selectedFilter.setData(responseModel.getHotels());
                selectedFilter.setTitleFa("هتل ها");
                break;
            case GYMS:
                selectedFilter.setData(responseModel.getGyms());
                selectedFilter.setTitleFa("باشگاه ورزشی");
                break;
            case PARKS:
                selectedFilter.setData(responseModel.getParks());
                selectedFilter.setTitleFa("پارک ها");
                break;
            case MOSQUES:
                selectedFilter.setData(responseModel.getMosques());
                selectedFilter.setTitleFa("مسجد ها");
                break;
            case SCHOOLS:
                selectedFilter.setData(responseModel.getSchools());
                selectedFilter.setTitleFa("مدرسه ها");
                break;
            case PARKINGS:
                selectedFilter.setData(responseModel.getParkings());
                selectedFilter.setTitleFa("پارکینگ ها");
                break;
            case HOSPITALS:
                selectedFilter.setData(responseModel.getHospitals());
                selectedFilter.setTitleFa("بیمارستان ها");
                break;
            case LIBRARIES:
                selectedFilter.setData(responseModel.getLibraries());
                selectedFilter.setTitleFa("کتابخانه ها");
                break;
            case BOOKSTORES:
                selectedFilter.setData(responseModel.getBookstores());
                selectedFilter.setTitleFa("کتابفروشی ها");
                break;
            case PHARMACIES:
                selectedFilter.setData(responseModel.getPharmacies());
                selectedFilter.setTitleFa("داروخانه ها");
                break;
            case FLOWER_SHOPS:
                selectedFilter.setData(responseModel.getFlowerShops());
                selectedFilter.setTitleFa("گل فروشی ها");
                break;
            case SHOPING_MALLS:
                selectedFilter.setData(responseModel.getShoppingMalls());
                selectedFilter.setTitleFa("پاساژ ها");
                break;
        }
        notifyObservers(selectedFilter);
    }

    public void setSelectedFilterType(PointType type){
        selectedFilter.setType(type);
        setChanged();
        pushFilteredData(latestResponseModel);
    }

    public Object getLatestResponseModel() {
        return latestResponseModel;
    }
}
