package ir.address.foo.base.data.api.general;

import ir.address.foo.base.data.api.ApiService;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.data.model.ResponseModel;
import retrofit2.Call;

public class GeneralApiRepository {

    public Call<ResponseModel<BasePoint>> getPoints(String lat, String lon) {
        return ApiService.getInstance().create(GeneralApi.class).getPoints(lat, lon);
    }
}
