package ir.address.foo.base.util;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.ViewPropertyTransition;

import java.io.File;

import ir.address.foo.app.App;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
public class ImageUtil {

    private static final ViewPropertyTransition.Animator animationObject = new ViewPropertyTransition.Animator() {
        @Override
        public void animate(View view) {
            view.setAlpha(0f);

            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(200);
            fadeAnim.start();
        }
    };

    public static void loadImage(String uri, final ImageView imageView, final int placeHolder) {
        if (uri == null || imageView == null)
            return;

        if (uri.toLowerCase().endsWith("default_pic.jpg")) {
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.with(App.getContext()).clear(imageView);
            // remove the placeholder (optional); read comments below
            imageView.setImageDrawable(null);
            return;
        }
        if (placeHolder != 0) {
            RequestOptions options = new RequestOptions()
                    .placeholder(placeHolder);
            Glide.with(App.getContext())
                    .load(uri)
                    .apply(options)
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        } else {
            Glide.with(App.getContext())
                    .load(uri)
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        }
    }

    public static void loadImage(final File file, final ImageView imageView, final int placeHolder) {
        if (file == null || imageView == null)
            return;

        if (file.getAbsolutePath().toLowerCase().endsWith("default_pic.jpg")) {
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.with(App.getContext()).clear(imageView);
            // remove the placeholder (optional); read comments below
            imageView.setImageDrawable(null);
            return;
        }
        if (placeHolder != 0) {
            RequestOptions options = new RequestOptions()
                    .placeholder(placeHolder);
            Glide.with(App.getContext())
                    .load(file)
                    .apply(options)
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        } else {
            Glide.with(App.getContext())
                    .load(file)
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        }
    }


    public static void loadImageRoundCorner(String uri, final ImageView imageView, final int placeHolder, int radius) {
        if (uri == null || imageView == null) {
            return;
        }
        if (uri.toLowerCase().endsWith("default_pic.jpg")) {
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.with(App.getContext()).clear(imageView);
            // remove the placeholder (optional); read comments below
            imageView.setImageDrawable(null);
            return;
        }
        if (placeHolder != 0) {
            RequestOptions options = new RequestOptions()
                    .placeholder(placeHolder);
            Glide.with(App.getContext())
                    .load(uri)
                    .apply(options)
                    .apply(bitmapTransform(new RoundedCornersTransformation(Utils.dpToPx(radius), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        } else {
            Glide.with(App.getContext())
                    .load(uri)
                    .apply(bitmapTransform(new RoundedCornersTransformation(Utils.dpToPx(radius), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        }
    }

    public static void loadDrawableImageRoundCorner(int resId, final ImageView imageView, final int placeHolder, int radius) {
        if (resId == 0 || imageView == null) {
            return;
        }
        if (placeHolder != 0) {
            RequestOptions options = new RequestOptions()
                    .placeholder(placeHolder);
            Glide.with(App.getContext())
                    .load(resId)
                    .apply(options)
                    .apply(bitmapTransform(new RoundedCornersTransformation(Utils.dpToPx(radius), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        } else {
            Glide.with(App.getContext())
                    .load(resId)
                    .apply(bitmapTransform(new RoundedCornersTransformation(Utils.dpToPx(radius), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        }
    }

    public static void loadImageRoundTopCorners(String uri, final ImageView imageView, final int placeHolder, int radius) {
        if (uri == null || imageView == null) {
            return;
        }
        if (uri.toLowerCase().endsWith("default_pic.jpg")) {
            // make sure Glide doesn't load anything into this view until told otherwise
            Glide.with(App.getContext()).clear(imageView);
            // remove the placeholder (optional); read comments below
            imageView.setImageDrawable(null);
            return;
        }
        if (placeHolder != 0) {
            RequestOptions options = new RequestOptions()
                    .placeholder(placeHolder);
            Glide.with(App.getContext())
                    .load(uri)
                    .apply(options)
                    .apply(bitmapTransform(new RoundedCornersTransformation(Utils.dpToPx(radius), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        } else {
            Glide.with(App.getContext())
                    .load(uri)
                    .apply(bitmapTransform(new RoundedCornersTransformation(Utils.dpToPx(radius), 0, RoundedCornersTransformation.CornerType.TOP)))
                    .transition(GenericTransitionOptions.with(animationObject))
                    .into(imageView);
        }
    }
}
