package ir.address.foo.base.data.model;

public enum PointType {
    RESTAURANTS ("restaurants"),
    CAFES ("cafes"),
    PARKS ("parks"),
    SHOPING_MALLS ("shopping_malls"),
    MOSQUES ("mosques"),
    PHARMACIES ("pharmacies"),
    HOSPITALS ("hospitals"),
    SCHOOLS ("schools"),
    GYMS ("gyms"),
    BOOKSTORES ("bookstores"),
    FLOWER_SHOPS ("flower_shops"),
    LIBRARIES ("libraries"),
    HOTELS ("hotels"),
    PARKINGS ("parkings"),
    HIGHWAYS ("highways"),
    BUS_STOPS ("bus_stops"),
    METRO_STATIONS ("metro_stations");

    private final String name;

    private PointType(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public boolean equals(PointType pointType) {
        return name.equals(pointType.name);
    }

    public String toString() {
        return this.name;
    }
}
