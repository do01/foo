package ir.address.foo.base.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.address.foo.R;
import ir.address.foo.app.Injection;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.data.model.ResponseModel;
import ir.address.foo.base.ui.components.MaterialProgressBar;
import ir.address.foo.base.util.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLngBounds.Builder mBounds;
    private LatLng mCurrentLocation;
    private int mCameraMoveReason;
    private boolean mFirstTime = true;

    @BindView(R.id.progressbar)
    MaterialProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFarsiLocate(this);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        mCurrentLocation = new LatLng(35.6935203, 51.3942589);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mFirstTime) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, 12));

        }
        loadMarkers(mCurrentLocation);
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                mCameraMoveReason = reason;
            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (mCameraMoveReason != GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    return;
                }
                mCurrentLocation = mMap.getCameraPosition().target;
                if (Utils.isNetworkConnected()) {
                    loadMarkers(mCurrentLocation);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadMarkers(LatLng latLng) {
        progressBar.setVisibility(View.VISIBLE);
        Call<ResponseModel<BasePoint>> call = Injection.provideGeneralApiRepository()
                .getPoints(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
        call.enqueue(new Callback<ResponseModel<BasePoint>>() {
            @Override
            public void onResponse(Call<ResponseModel<BasePoint>> call, Response<ResponseModel<BasePoint>> response) {
                if (response.body() != null && response.body().getRestaurants().size() > 0) {
                    // Cleaning all the markers.
                    if (mMap != null) {
                        mMap.clear();
                    }
                    mBounds = new LatLngBounds.Builder();
                    for (BasePoint basePoint : response.body().getRestaurants()) {
                        LatLng latLng = new LatLng(
                                Double.valueOf(basePoint.getLat()),
                                Double.valueOf(basePoint.getLng())
                        );
//                        if(mMap.getProjection().getVisibleRegion().latLngBounds.contains(latLng)) {
                        mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(basePoint.getTitle())
                                .icon(Utils.bitmapDescriptorFromVector(MapsActivity.this, R.drawable.marker)));
                        if(mFirstTime) {
                            mBounds.include(latLng);
                        }
//                        }
                    }
                    if(mFirstTime) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(mBounds.build(), (int) mMap.getCameraPosition().zoom));
                        mFirstTime = false;
                    }
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<BasePoint>> call, Throwable t) {

            }
        });
    }

}
