package ir.address.foo.base.data.api.general;

import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.data.model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GeneralApi {

    @GET("hmj/by/point/{lat}/{lon}")
    Call<ResponseModel<BasePoint>> getPoints(@Path("lat") String lat, @Path("lon") String lon);
}