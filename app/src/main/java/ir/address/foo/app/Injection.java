package ir.address.foo.app;


import ir.address.foo.base.data.api.general.GeneralApiRepository;
import ir.address.foo.base.data.repository.NearbyDataRepository;

public class Injection {

    public static GeneralApiRepository provideGeneralApiRepository(){
        return new GeneralApiRepository();
    }

    public static NearbyDataRepository provideNearbyDataRepository(){
        return NearbyDataRepository.getInstance();
    }
}
