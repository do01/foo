package ir.address.foo.app;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;

import com.bumptech.glide.Glide;

public class App extends MultiDexApplication {

    private static App sInstance;
    private static SharedPreferences sSharedPrefs;
    private static Resources sResources;

    @Override
    public void onCreate() {
        super.onCreate();

        // general config
        sInstance = this;
        sSharedPrefs = getSharedPrefs();
        sResources = getAppResources();

        // svg image support
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        // glide
        Glide.get(this).clearMemory();
    }

    public static synchronized App getInstance() {
        return sInstance;
    }

    public static synchronized Context getContext() {
        return sInstance.getApplicationContext();
    }

    public static synchronized Resources getAppResources() {
        if(sResources == null){
            sResources = getContext().getResources();
        }
        return sResources;
    }
    public SharedPreferences getSharedPrefs() {
        if (sSharedPrefs == null)
            sSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sSharedPrefs;
    }
}
