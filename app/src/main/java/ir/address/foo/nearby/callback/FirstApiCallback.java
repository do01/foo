package ir.address.foo.nearby.callback;

public interface FirstApiCallback {
    void onFirstCallDone();
}
