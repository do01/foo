package ir.address.foo.nearby.fragment;

import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

import ir.address.foo.R;
import ir.address.foo.app.App;
import ir.address.foo.app.Injection;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.base.util.Utils;
import ir.address.foo.base.data.repository.NearbyDataRepository;

public class NearbyFragmentPresenter implements Observer, OnMapReadyCallback {

    private NearbyFragmentView view;
    private GoogleMap mMap;
    private LatLngBounds.Builder mBounds;
    private LatLng mCurrentLocation;
    private int mCameraMoveReason;
    private Boolean mFirstLoad = true;
    private NearbyDataRepository nearbyDataRepository;

    public NearbyFragmentPresenter(NearbyFragmentView view){
        this.view = view;
        nearbyDataRepository = Injection.provideNearbyDataRepository();
        this.update(nearbyDataRepository,  Injection.provideNearbyDataRepository().getLatestResponseModel());
    }

    public void setupMap(FragmentManager manager) {
        // initial point center of tehran
        mCurrentLocation = new LatLng(35.6935203, 51.3942589);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment =
                (SupportMapFragment) manager.findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mFirstLoad) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, 12));
        }
        mMap.setOnCameraMoveStartedListener(reason -> mCameraMoveReason = reason);

        mMap.setOnCameraIdleListener(() -> {
            if (mCameraMoveReason != GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                return;
            }
            mCurrentLocation = mMap.getCameraPosition().target;
            if (Utils.isNetworkConnected()) {
                loadMarkers(mCurrentLocation);
            } else {
                Toast.makeText(App.getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMarkers(LatLng latLng) {
        view.showProgressBar();
        Injection.provideNearbyDataRepository().loadPoints(latLng);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof Filter){
            Filter filter = ((Filter)o);
            view.updateBottomsheetHeader(filter);
            view.showMarkers(mMap, filter.getData(), mBounds, mFirstLoad, () -> mFirstLoad = false);
        }
        nearbyDataRepository.addObserver(this);
    }

    public void destroy() {
        nearbyDataRepository.deleteObserver(this);
    }
}
