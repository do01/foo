package ir.address.foo.nearby.fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;

import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.nearby.callback.FirstApiCallback;

public interface NearbyFragmentView {
    void showProgressBar();
    void showMarkers(GoogleMap map,
                     List<BasePoint> points,
                     LatLngBounds.Builder bounds,
                     Boolean firstLoad,
                     FirstApiCallback firstApiCallback);

    void updateBottomsheetHeader(Filter filter);
}
