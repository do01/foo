package ir.address.foo.nearby.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.address.foo.R;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.ui.fragments.BaseFragment;
import ir.address.foo.base.ui.components.MaterialProgressBar;
import ir.address.foo.base.util.Utils;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.nearby.callback.FirstApiCallback;
import ir.address.foo.related.RelatedFragment;

public class NearbyFragment extends BaseFragment implements NearbyFragmentView {
    public final static String TAG = NearbyFragment.class.getSimpleName();

    @BindView(R.id.main_content)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.bottom_sheet)
    View bottomSheet;
    @BindView(R.id.root_container)
    ConstraintLayout rootContainer;
    @BindView(R.id.map_container)
    ConstraintLayout mapContainer;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressBar;
    @BindView(R.id.bottom_sheet_title_tv)
    TextView bottomsheetTitleTv;
    @BindView(R.id.bottom_sheet_count_details_tv)
    TextView getBottomsheetCountDetailsTv;


    private NearbyFragmentPresenter presenter;

    public static NearbyFragment getInstance(){
        NearbyFragment fragment = new NearbyFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_nearby, container, false);
        ButterKnife.bind(this, view);

        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                } else {}
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        rootContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rootContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                behavior.setPeekHeight(rootContainer.getHeight() / 3);

            }
        });

        presenter = new NearbyFragmentPresenter(this);
        presenter.setupMap(getChildFragmentManager());

        // commit filter fragment
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        BaseFragment relatedFragment = RelatedFragment.getInstance();
        transaction.replace(R.id.related_fragment_container, relatedFragment, RelatedFragment.TAG);
        transaction.commitAllowingStateLoss();

        return view;
    }

    @Override
    public boolean onBackPressed() {
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        if(behavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return true;
        } else {
            return super.onBackPressed();
        }
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMarkers(GoogleMap map,
                            List<BasePoint> points,
                            LatLngBounds.Builder bounds,
                            Boolean firstLoad,
                            FirstApiCallback callback) {
        // Cleaning all the markers.
        if (map != null) {
            map.clear();
        }else{
            return;
        }
        // add markers
        if(points == null){
            return;
        }
        bounds = new LatLngBounds.Builder();
        for (BasePoint basePoint : points) {
            LatLng latLng = new LatLng(
                    Double.valueOf(basePoint.getLat()),
                    Double.valueOf(basePoint.getLng())
            );
//                        if(mMap.getProjection().getVisibleRegion().latLngBounds.contains(latLng)) {
            map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(basePoint.getTitle())
                    .icon(Utils.bitmapDescriptorFromVector(NearbyFragment.this.getContext(), R.drawable.marker)));
            if(firstLoad) {
                bounds.include(latLng);
            }
//                        }
        }
        if(firstLoad) {
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), (int) map.getCameraPosition().zoom));
            if (callback != null) {
                callback.onFirstCallDone();
            }
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateBottomsheetHeader(Filter filter) {
        bottomsheetTitleTv.setText(filter.getTitleFa());

        String countPointsString = "۰" + " " + getString(R.string.point_count_found);
        if(filter.getData() != null) {
            countPointsString = filter.getData().size() + " " + getString(R.string.point_count_found);
        }
        getBottomsheetCountDetailsTv.setText(countPointsString);
    }

    @Override
    public void onDetach() {
        presenter.destroy();
        super.onDetach();
    }
}
