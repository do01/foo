package ir.address.foo.related.listener;

import ir.address.foo.base.data.model.BasePoint;

public interface OnRelatedItemClickListener {
    void onClick(BasePoint basePoint);
}
