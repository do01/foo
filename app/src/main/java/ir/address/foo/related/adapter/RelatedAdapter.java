package ir.address.foo.related.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.address.foo.R;
import ir.address.foo.app.App;
import ir.address.foo.base.util.ImageUtil;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.related.listener.OnRelatedItemClickListener;

public class RelatedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = RelatedAdapter.class.getSimpleName();

    private List<BasePoint> mPoints;
    private OnRelatedItemClickListener mOnItemClickListener;

    public RelatedAdapter(List<BasePoint> points, OnRelatedItemClickListener onItemClickListener) {
        mPoints = points;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        final RelatedItemHolder holder = new RelatedItemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.related_item, parent, false));
        holder.itemView.setOnClickListener(view -> itemClicked(holder.getAdapterPosition()));
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        setupLiveChannelView((RelatedItemHolder) holder, position);
    }

    @Override
    public int getItemCount() {
        if (mPoints != null) {
            return mPoints.size();
        } else {
            return 0;
        }
    }

    private void setupLiveChannelView(RelatedItemHolder relatedHolder, int position) {
        final BasePoint basePoint = getRelatedItem(position);
        if (basePoint != null) {
            relatedHolder.relatedTitleTv.setText(basePoint.getTitle());
            relatedHolder.relatedDetailsTv.setText(App.getAppResources().getText(R.string.related_default_details));
            int resId = App.getAppResources().getIdentifier("image", "drawable", App.getContext().getPackageName());
            ImageUtil.loadDrawableImageRoundCorner(resId, relatedHolder.relatedIv, 0, 10);
        }
    }

    private BasePoint getRelatedItem(int position) {
        if (mPoints != null)
            return mPoints.get(position);
        else
            return null;
    }

    private void itemClicked(int position) {
        if (mOnItemClickListener != null)
            mOnItemClickListener.onClick(getRelatedItem(position));
    }

    public void setItems(List<BasePoint> markers) {
        mPoints = markers;
    }

    private static class RelatedItemHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout relatedCl;
        private ImageView relatedIv;
        private TextView relatedTitleTv;
        private TextView relatedDetailsTv;

        RelatedItemHolder(final View itemView) {
            super(itemView);
            relatedIv = itemView.findViewById(R.id.related_iv);
            relatedTitleTv = itemView.findViewById(R.id.related_title_tv);
            relatedDetailsTv = itemView.findViewById(R.id.related_details_tv);
            relatedCl = itemView.findViewById(R.id.related_cl);
        }
    }

}
