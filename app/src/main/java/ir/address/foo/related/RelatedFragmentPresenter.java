package ir.address.foo.related;

import java.util.Observable;
import java.util.Observer;

import ir.address.foo.app.Injection;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.base.data.repository.NearbyDataRepository;

public class RelatedFragmentPresenter implements Observer {

    private RelatedFragmentView view;
    private NearbyDataRepository nearbyDataRepository;


    public RelatedFragmentPresenter(RelatedFragmentView view){
        this.view = view;
        nearbyDataRepository = Injection.provideNearbyDataRepository();
        this.update(nearbyDataRepository,  Injection.provideNearbyDataRepository().getLatestResponseModel());
        view.showProgress();
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof Filter){
            view.showMarkers(((Filter)o).getData());
        }
        nearbyDataRepository.addObserver(this);
    }

    public void destroy() {
        nearbyDataRepository.deleteObserver(this);
    }
}
