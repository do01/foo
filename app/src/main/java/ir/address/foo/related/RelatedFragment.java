package ir.address.foo.related;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.address.foo.R;
import ir.address.foo.base.ui.components.MaterialProgressBar;
import ir.address.foo.base.ui.fragments.BaseFragment;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.related.adapter.RelatedAdapter;
import ir.address.foo.related.listener.OnRelatedItemClickListener;

public class RelatedFragment extends BaseFragment implements RelatedFragmentView {
    public static final String TAG = RelatedFragment.class.getSimpleName();

    @BindView(R.id.related_rv)
    RecyclerView relatedRv;
    @BindView(R.id.progressbar)
    MaterialProgressBar progressBar;

    private RelatedFragmentPresenter presenter;

    public static RelatedFragment getInstance(){
        RelatedFragment relatedFragment = new RelatedFragment();
        relatedFragment.setArguments(new Bundle());
        return relatedFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_related, container, false);
        ButterKnife.bind(this, view);

        presenter = new RelatedFragmentPresenter(this);
        return view;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMarkers(List<BasePoint> markers) {
        progressBar.setVisibility(View.GONE);
        if(getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (relatedRv != null && relatedRv.getAdapter() != null) {
                    ((RelatedAdapter) relatedRv.getAdapter()).setItems(markers);
                    relatedRv.getAdapter().notifyDataSetChanged();
                } else if (markers != null && markers.size() > 0) {
                    RelatedAdapter gridAdapter = new RelatedAdapter(markers, new OnRelatedItemClickListener() {
                        @Override
                        public void onClick(BasePoint basePoint) {

                        }
                    });
                    LinearLayoutManager llm = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    relatedRv.setHasFixedSize(true);
                    relatedRv.setLayoutManager(llm);
                    relatedRv.setAdapter(gridAdapter);
                    relatedRv.setNestedScrollingEnabled(false);
                    relatedRv.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }
}
