package ir.address.foo.related;

import java.util.List;

import ir.address.foo.base.data.model.BasePoint;

public interface RelatedFragmentView {
    void showProgress();
    void showMarkers(List<BasePoint> markers);
}
