package ir.address.foo.filter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.address.foo.R;
import ir.address.foo.base.ui.fragments.BaseFragment;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.filter.adapter.CarouselAdapter;
import ir.address.foo.filter.callback.OnFilterItemClickListener;
import ir.address.foo.main.fragment.listeners.OnFilterChangeListener;

public class FilterFragment extends BaseFragment implements FilterFragmentView {
    public static final String TAG = FilterFragment.class.getSimpleName();

    @BindView(R.id.filter_rv)
    RecyclerView filterRv;
    private OnFilterChangeListener onFilterChangeListener;
    private FilterFragmentPresenter presenter;
    private CarouselAdapter adapter;

    public static FilterFragment getInstance() {
        FilterFragment filterFragment = new FilterFragment();
        filterFragment.setArguments(new Bundle());
        return filterFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onFilterChangeListener = (OnFilterChangeListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MyInterface ");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_filter, container, false);
        ButterKnife.bind(this, view);

        presenter = new FilterFragmentPresenter(this);

        return view;
    }

    @Override
    public void showFilters(List<Filter> filters){
        if(filterRv != null && filterRv.getAdapter() != null){
            filterRv.getAdapter().notifyDataSetChanged();
        }else {
            adapter = new CarouselAdapter(filters);
            adapter.setOnFilterItemClickListener(new OnFilterItemClickListener() {
                @Override
                public void onClick(Filter filter) {
                    presenter.changeNearbyFilter(filter);
                }
            });
            LinearLayoutManager lm = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);
            filterRv.setHasFixedSize(true);
            filterRv.setNestedScrollingEnabled(false);
            filterRv.setLayoutManager(lm);
            filterRv.setAdapter(adapter);
        }
    }

    @Override
    public void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }
}
