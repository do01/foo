package ir.address.foo.filter;

import java.util.List;

import ir.address.foo.base.data.model.Filter;

public interface FilterFragmentView {
    void showFilters(List<Filter> filters);
}
