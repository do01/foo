package ir.address.foo.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import ir.address.foo.app.Injection;
import ir.address.foo.base.data.model.BasePoint;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.base.data.model.PointType;
import ir.address.foo.base.data.model.ResponseModel;
import ir.address.foo.base.data.repository.NearbyDataRepository;

public class FilterFragmentPresenter implements Observer {

    private FilterFragmentView view;
    private NearbyDataRepository nearbyDataRepository;


    public FilterFragmentPresenter(FilterFragmentView view){
        this.view = view;
        nearbyDataRepository = Injection.provideNearbyDataRepository();
        this.update(nearbyDataRepository,  Injection.provideNearbyDataRepository().getLatestResponseModel());
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof ResponseModel){
            ResponseModel<BasePoint> responseModel = (ResponseModel<BasePoint>)o;
            List<Filter> filters = new ArrayList<>();
            if(responseModel.getRestaurants() != null && !responseModel.getRestaurants().isEmpty()){
                filters.add(new Filter("رستوران", PointType.RESTAURANTS));
            }
            if(responseModel.getCafes() != null && !responseModel.getCafes().isEmpty()){
                filters.add(new Filter("کافه", PointType.CAFES));
            }
            if(responseModel.getHotels() != null && !responseModel.getHotels().isEmpty()){
                filters.add(new Filter("هتل", PointType.HOTELS));
            }
            if(responseModel.getHospitals() != null && !responseModel.getHospitals().isEmpty()){
                filters.add(new Filter("بیمارستان", PointType.HOSPITALS));
            }
            if(responseModel.getSchools() != null && !responseModel.getSchools().isEmpty()){
                filters.add(new Filter("مدرسه", PointType.SCHOOLS));
            }
            if(responseModel.getPharmacies() != null && !responseModel.getPharmacies().isEmpty()){
                filters.add(new Filter("داروخانه", PointType.PHARMACIES));
            }
            if(responseModel.getBookstores() != null && !responseModel.getBookstores().isEmpty()){
                filters.add(new Filter("کتاب فروشی", PointType.BOOKSTORES));
            }
            if(responseModel.getParkings() != null && !responseModel.getParkings().isEmpty()){
                filters.add(new Filter("پارکینگ", PointType.PARKINGS));
            }
            if(responseModel.getMosques() != null && !responseModel.getMosques().isEmpty()){
                filters.add(new Filter("مسجد", PointType.MOSQUES));
            }
            if(responseModel.getGyms() != null && !responseModel.getGyms().isEmpty()){
                filters.add(new Filter("باشگاه ورزشی", PointType.GYMS));
            }
            if(responseModel.getFlowerShops() != null && !responseModel.getFlowerShops().isEmpty()){
                filters.add(new Filter("گل فروشی", PointType.FLOWER_SHOPS));
            }
            if(responseModel.getLibraries() != null && !responseModel.getLibraries().isEmpty()){
                filters.add(new Filter("کتابخانه", PointType.LIBRARIES));
            }
            if(responseModel.getShoppingMalls() != null && !responseModel.getShoppingMalls().isEmpty()){
                filters.add(new Filter("پاساژ", PointType.SHOPING_MALLS));
            }
            if(responseModel.getParks() != null && !responseModel.getParks().isEmpty()){
                filters.add(new Filter("پارک", PointType.PARKS));
            }
            if(responseModel.getHighways() != null && !responseModel.getHighways().isEmpty()){
                filters.add(new Filter("بزرگراه", PointType.HIGHWAYS));
            }
            if(responseModel.getBusStops() != null && !responseModel.getBusStops().isEmpty()){
                filters.add(new Filter("ایستگاه اتوبوس", PointType.BUS_STOPS));
            }
            if(responseModel.getMetroStations() != null && !responseModel.getMetroStations().isEmpty()){
                filters.add(new Filter("ایستگاه مترو", PointType.METRO_STATIONS));
            }
            view.showFilters(filters);
        }
        nearbyDataRepository.addObserver(this);
    }

    private void prepareFilters(){

    }

    public void changeNearbyFilter(Filter filter) {
        nearbyDataRepository.setSelectedFilterType(filter.getType());
    }

    public void destroy() {
        nearbyDataRepository.deleteObserver(this);
    }
}
