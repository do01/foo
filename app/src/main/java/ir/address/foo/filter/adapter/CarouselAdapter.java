package ir.address.foo.filter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.address.foo.R;
import ir.address.foo.base.data.model.Filter;
import ir.address.foo.base.util.Utils;
import ir.address.foo.filter.callback.OnFilterItemClickListener;

public class CarouselAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG = CarouselAdapter.class.getSimpleName();

    private List<Filter> mFilters;
    private OnFilterItemClickListener mOnFilterItemClickListener;
    private int selectedPosition = 0;

    public CarouselAdapter(List<Filter> filters) {
        mFilters = filters;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final FilterHolder filterHolder = new FilterHolder(layoutInflater.inflate(R.layout.filter_item, parent, false));
        filterHolder.itemView.setOnClickListener(view -> {
            // Below line is just like a safety check, because sometimes holder could be null,
            // in that case, getAdapterPosition() will return RecyclerView.NO_POSITION
            if (filterHolder.getAdapterPosition() == RecyclerView.NO_POSITION) return;
            // Updating old as well as new positions
            notifyItemChanged(selectedPosition);
            selectedPosition = filterHolder.getAdapterPosition();
            notifyItemChanged(selectedPosition);
            if (mOnFilterItemClickListener != null)
                mOnFilterItemClickListener.onClick(getFilterItem(filterHolder.getAdapterPosition()));

        });
        return filterHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        FilterHolder filterHolder = (FilterHolder) holder;
        final Filter filter = getFilterItem(position);
        if (filter == null)
            return;

        final String filterTitle = filter.getTitleFa();
        filterHolder.filterTitleTv.setText(filterTitle);
        if (selectedPosition == position) {
            filterHolder.filterTitleTv.setTextColor(ContextCompat.getColor(filterHolder.itemView.getContext(), R.color.filter_selected_text));
            filterHolder.filterTitleTv.setTypeface(ResourcesCompat.getFont(filterHolder.itemView.getContext(), R.font.iran_sans_bold));
        } else {
            filterHolder.filterTitleTv.setTextColor(ContextCompat.getColor(filterHolder.itemView.getContext(), R.color.filter_normal_text));
            filterHolder.filterTitleTv.setTypeface(ResourcesCompat.getFont(filterHolder.itemView.getContext(), R.font.iran_sans_medium));
        }

        if (position == 0) {
            filterHolder.itemView.setPadding(0, 0, Utils.dpToPx(4), 0);
        } else if (position == mFilters.size() - 1) {
            filterHolder.itemView.setPadding(Utils.dpToPx(4), 0, 0, 0);
        } else {
            filterHolder.itemView.setPadding(0, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return mFilters.size();
    }

    public OnFilterItemClickListener getOnFilterItemClickListener() {
        return mOnFilterItemClickListener;
    }

    public void setOnFilterItemClickListener(OnFilterItemClickListener onFilterItemClickListener) {
        this.mOnFilterItemClickListener = onFilterItemClickListener;
    }

    private Filter getFilterItem(int position) {
        if (mFilters != null && mFilters.size() > 0)
            return mFilters.get(position);
        else
            return null;
    }

    private static class FilterHolder extends RecyclerView.ViewHolder {
        private TextView filterTitleTv;

        FilterHolder(View itemView) {
            super(itemView);
            filterTitleTv = itemView.findViewById(R.id.filter_title_tv);
        }
    }
}
