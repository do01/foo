package ir.address.foo.filter.callback;

import ir.address.foo.base.data.model.Filter;

public interface OnFilterItemClickListener {
    void onClick(Filter filter);
}
